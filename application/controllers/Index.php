<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct(){
        parent:: __construct();
        $this->load->helper('form');
    }
	public function index()
	{   
        $data['mensaje']="";
		$this->load->view('login_view',$data);
    }

    public function recibir_datos_login_form(){
        
        if($this->validar_login_form()){
            $data['correo'] =  $this->input->post('correo');
            $this->load->model('profesores_model');
            $data['profesor']= $this->profesores_model->get_profesor_by_correo($data);
            if($data['profesor']){
                $data['profesor'] = $data['profesor'][0];
                $this->load->model('grupos_model');
                $data['grupos']=$this->grupos_model->get_grupos_by_profesor_correo($data);
                $data['mensaje'] = "";
                $this->load->view('grupos_view',$data);
            }else{
                $data['mensaje'] = "correo sin acceso";
                $this->load->view('login_view',$data);

            }            
        }else{
            $data['mensaje'] = validation_errors();
            $this->load->view('login_view',$data);

        }

    }

    public function recibir_grupo($grupo_id){
        $data['grupo_id']=$grupo_id;
        if($data['grupo_id']){
            $this->load->model('grupos_model');
            $data['materias'] = $this->grupos_model->get_materias_by_grupo_id($data);
            $data['mensaje']= "";
            $this->load->view('materias_view',$data);
            
        }else{
            $data['mensaje'] = "falta parametro grupo id";
            $this->load->view('materias_view',$data);
        }

    } 
    public function recibir_materia($grupo_id,$materia_id){
        $data['materia_id']=$materia_id;
        $data['grupo_id']= $grupo_id;
        $this->load->model('rubricas_model');
        $data['curso_id']= $this->rubricas_model->get_curso_id_by_materia_grupo_id($data);
        $data['curso_id']=$data['curso_id'][0]['id'];
        if($data['materia_id'] && $data['grupo_id'] && $data['curso_id']){
            $data['mensaje'] = "";
            $this->load->view('menu_materia_view',$data);

        }else{
            $data['mensaje'] = "falta parametro materia_id o grupo_id o curso_id";
            $this->load->view('menu_materia_view',$data);
        }
    }

    public function recibir_menu_rubrica($curso_id,$grupo_id,$materia_id){
        $data['materia_id'] = $materia_id;
        $data['grupo_id'] = $grupo_id;
        $data['curso_id'] =$curso_id;
        $this->load->model('rubricas_model');
        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
        $data['mensaje'] = "";
        $this->load->view('mostrar_rubricas_view',$data);

    
        
    }

    public function mostrar_rubrica_form($curso_id,$grupo_id,$materia_id){
        $data['materia_id'] = $materia_id;
        $data['grupo_id'] = $grupo_id;
        $data['curso_id'] = $curso_id;
        $data['mensaje'] = "";
        $this->load->view('agregar_rubrica_form_view',$data);
    }

    public function insertar_rubrica(){
        if($this->validar_rubrica_form()){
            $this->load->model('rubricas_model');
            $data['rubrica']= $this->input->post('rubrica');
            $data['ponderacion'] = $this->input->post('ponderacion');
            $data['materia_id']= $this->input->post('materia_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['curso_id'] = $this->input->post('curso_id');
            $ponderacion=$this->rubricas_model->get_suma_de_ponderaciones_by_curso_id($data);
            $ponderacion_total= $data['ponderacion'] + $ponderacion[0]['Sum(rubrica_evaluacion.ponderacion)'];
            if(!($ponderacion_total > 100)){
            
                if($this->rubricas_model->insertar_rubrica($data)){
                    $data['mensaje'] = "insertado correctamente";
                    $this->load->view('agregar_rubrica_form_view',$data);
                }else{
                    
                    $data['mensaje'] = "error al insertar rubrica";
                    $this->load->view('agregar_rubrica_form_view',$data);
                }
            }else{
                $data['mensaje'] = "la ponderacion no debe ser mayor al 100%";
                $this->load->view('agregar_rubrica_form_view',$data);
            }
        }else{
            $data['materia_id']= $this->input->post('materia_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['mensaje'] = validation_errors();
            $data['curso_id'] = $this->rubricas_model->get_curso_id_by_materia_grupo_id($data);
            $data['curso_id'] = $data['curso_id'][0]['id'];
            $this->load->view('agregar_rubrica_form_view',$data);
        }
    }


    public function editar_rubrica_view($curso_id,$grupo_id,$materia_id,$rubrica_id){
            $data['rubrica_id']= $rubrica_id;
            $data['grupo_id']= $grupo_id;
            $data['materia_id']= $materia_id;
            $data['curso_id']= $curso_id;

            if($data['rubrica_id'] && $data['grupo_id'] && $data['materia_id'] && $data['curso_id']){
                $data['mensaje']="";
                $this->load->model('rubricas_model');
                $data['rubrica']=$this->rubricas_model->get_rubrica_by_id($data);
                $data['rubrica'] = $data['rubrica'][0];
                $this->load->view('editar_rubrica_form_view',$data);
                
            }else{
                $data['mensaje']="falta parametro";
                $this->load->view('editar_rubrica_form_view',$data);
                
            }
    }

    public function update_rubrica(){

        if($this->validar_rubrica_form()){
            $data['rubrica']= $this->input->post('rubrica');
            $data['ponderacion'] = $this->input->post('ponderacion');
            $data['rubrica_id']= $this->input->post('rubrica_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['curso_id']= $this->input->post('curso_id');
            $this->load->model('rubricas_model');
            if($this->rubricas_model->update_rubrica_by_id($data)){ 
                $data['mensaje'] = "Actualizado correctamente";
                $this->load->view('editar_rubrica_form_view',$data);              
            }else{
                $data['rubrica_id']= $this->input->post('rubrica_id');
                $data['grupo_id']= $this->input->post('grupo_id');
                $data['materia_id']= $this->input->post('materia_id');
                $data['curso_id']= $this->input->post('curso_id');
                $data['mensaje'] = "error al actualizar rubrica";
                $this->load->view('editar_rubrica_form_view',$data);
            }
        }else{
            $data['rubrica_id']= $this->input->post('rubrica_id');
            $data['mensaje'] = validation_errors();
            $this->load->model('rubricas_model');
            $data['rubrica']=$this->rubricas_model->get_rubrica_by_id($data);
            $data['rubrica'] = $data['rubrica'][0];
            $this->load->view('editar_rubrica_form_view',$data);
        }
    }

    public function borrar_rubrica($curso_id,$grupo_id,$materia_id,$rubrica_id){
            $data['rubrica_id']= $rubrica_id;
            $data['rubrica_evaluacion_id'] = $rubrica_id;
            $data['grupo_id']= $grupo_id;
            $data['materia_id']= $materia_id;
            $data['curso_id']= $curso_id;
            if($data['rubrica_id']){
                $this->load->model('evaluaciones_model');
                $this->load->model('alumnos_evaluaciones_model');
                $bandera=TRUE;
                $data['evaluaciones'] = $this->evaluaciones_model->get_evaluaciones_by_rubrica_evaluacion_id($data);
                foreach($data['evaluaciones'] as $evaluacion){
                    $evaluacion['evaluacion_id'] =$evaluacion['id'];
                    if($this->alumnos_evaluaciones_model->borrar_alumnos_evaluaciones_by_evaluacion_id($evaluacion)){
                        
                    }else{
                        $bandera=FALSE;
                    }
                    
                }
                if($bandera && $this->evaluaciones_model->borrar_evaluaciones_by_rubrica_evaluaciones_id($data)){
                    $this->load->model('rubricas_model');
                    if($this->rubricas_model->borrar_rubrica_by_id($data)){
                        $data['mensaje'] = "rubrica eliminada";
                        $this->load->model('rubricas_model');
                        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
                        $this->load->view('mostrar_rubricas_view',$data);
                    }else{
                        $this->load->model('rubricas_model');
                        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
                        $data['mensaje'] = "no se pudo borrar la rubrica";
                        $this->load->view('mostrar_rubricas_view',$data);
                    }                 
                }else{
                    $data['mensaje'] = "error al borrar evaluaciones";
                    $this->load->view('mostrar_rubricas_view',$data);
                }
            }else{
                $data['mensaje'] = "falta parametro rubrica_id";
                $this->load->view('mostrar_rubricas_view',$data);
            }

    }

    ///////////////////////////////////////inicio para agregar evaluacions//////////////////////////////////////////////////////////////////////////////
    public function mostrar_evaluaciones($curso_id,$grupo_id,$materia_id){
        $data['curso_id'] = $curso_id;
        $data['grupo_id']= $grupo_id;
        $data['materia_id']= $materia_id;
        $this->load->model('evaluaciones_model');
        $data['evaluaciones']=$this->evaluaciones_model->get_all_evaluaciones_by_curso_id($data);
        $data['mensaje'] = "";
        $this->load->view('mostrar_evaluaciones_view',$data);

    }
    public function mostrar_agregar_evaluacion_form($curso_id,$grupo_id,$materia_id){
        $data['curso_id'] = $curso_id;
        $data['grupo_id']= $grupo_id;
        $data['materia_id']= $materia_id;
        $this->load->model('rubricas_model');
        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
        $data['mensaje'] = "";
        $this->load->view('agregar_evaluacion_form_view',$data);
        
    }
    public function agregar_evaluacion(){
        if($this->validar_evaluacion_form()){    
                $data['rubrica_evaluacion_id']= $this->input->post('rubrica_evaluacion_id');
                $data['evaluacion'] = $this->input->post('evaluacion');
                $data['grupo_id']= $this->input->post('grupo_id');
                $data['materia_id']= $this->input->post('materia_id');
                $data['curso_id']= $this->input->post('curso_id');
                $this->load->model('evaluaciones_model');
                $data['ultima_evaluacion_id'] = $this->evaluaciones_model->insertar_evaluacion($data);
                if($data['ultima_evaluacion_id']){
                    $this->load->model('alumnos_model');
                    $data['alumnos'] = $this->alumnos_model->get_all_alumnos_by_curso_id($data);
                    $this->load->model('alumnos_evaluaciones_model');
                    $bandera=FALSE;
                    foreach($data['alumnos'] as $alumno_id){
                        if($this->alumnos_evaluaciones_model->insertar_evaluacion_x_alumno($alumno_id,$data)){
                            $bandera=TRUE;
                        }else{
                            $bandera=FALSE;
                        }
                    }
                    if($bandera){
                        $data['mensaje']= "evaluacion agregada correctamente";
                        $this->load->model('rubricas_model');
                        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
                        $this->load->view('agregar_evaluacion_form_view',$data);
                    }
                    else{
                        $data['mensaje'] = "Error al insertar evaluacion";
                        $this->load->model('rubricas_model');
                        $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
                        $this->load->view('agregar_evaluacion_form_view',$data);
                    }
                }else{
                    $data['mensaje'] = "Error al insertar evaluacion";
                    $this->load->model('rubricas_model');
                    $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
                    $this->load->view('agregar_evaluacion_form_view',$data);
                }
        }else{
            $data['mensaje'] = validation_errors();
            $data['rubrica_evaluacion_id']= $this->input->post('rubrica_evaluacion_id');
            $data['evaluacion'] = $this->input->post('evaluacion');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['curso_id']= $this->input->post('curso_id');
            $this->load->model('rubricas_model');
            $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data);
            $this->load->view('agregar_evaluacion_form_view',$data);
        }
    }

    public function editar_evaluacion_form($curso_id,$grupo_id,$materia_id,$evaluacion_id){
        $data['evaluacion_id']= $evaluacion_id;
        $data['grupo_id']= $grupo_id;
        $data['materia_id']= $materia_id;
        $data['curso_id']= $curso_id;
        if($data['grupo_id'] && $data['materia_id'] && $data['curso_id'] && $data['evaluacion_id']){
            $data['mensaje']="";
            $this->load->model('evaluaciones_model');
            $data['evaluacion']=$this->evaluaciones_model->get_evaluacion_by_id($data); //se obtienen los datos de la evaluacion seleccionada
            $data['evaluacion'] = $data['evaluacion'][0];
            $this->load->model('rubricas_model');
            $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data); //todas las rubricas del curso
            $this->load->view('editar_evaluacion_form_view',$data);
        }else{
            $data['mensaje']="falta parametro";
            $this->load->view('editar_evaluacion_form_view',$data);
        }  

    }
    public function editar_evaluacion(){
        if($this->validar_evaluacion_form()){
            $data['rubrica_evaluacion_id'] = $this->input->post('rubrica_evaluacion_id');
            $data['evaluacion']= $this->input->post('evaluacion');
            $data['evaluacion_id']= $this->input->post('evaluacion_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['curso_id']= $this->input->post('curso_id');
            $this->load->model('evaluaciones_model');
            if($this->evaluaciones_model->update_evaluacion_by_id($data)){ 
                $data['mensaje'] = "Actualizado correctamente";
                $this->load->model('rubricas_model');
                $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data); //todas las rubricas del curso
                $this->load->view('editar_evaluacion_form_view',$data);              
            }else{
                $data['mensaje'] = "error al actualizar evaluacion";
                $this->load->model('rubricas_model');
                $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data); //todas las rubricas del curso
                $this->load->view('editar_evaluacion_form_view',$data);
            }
        }else{
            $data['rubrica_evaluacion_id'] = $this->input->post('rubrica_evaluacion_id');
            $data['evaluacion']= $this->input->post('evaluacion');
            $data['evaluacion_id']= $this->input->post('evaluacion_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['curso_id']= $this->input->post('curso_id');
            $data['mensaje'] = validation_errors();
            $this->load->model('evaluaciones_model');
            $data['evaluacion']=$this->evaluaciones_model->get_evaluacion_by_id($data);//se obtienen los datos de la evaluacion seleccionada
            $data['evaluacion'] = $data['evaluacion'][0];
            $this->load->model('rubricas_model');
            $data['rubricas']=$this->rubricas_model->get_rubrica_by_curso_id($data); //todas las rubricas del curso
            $this->load->view('editar_evaluacion_form_view',$data);
        }
    }


    public function borrar_evaluacion($curso_id,$grupo_id,$materia_id,$evaluacion_id){
        $data['evaluacion_id']= $evaluacion_id;
        $data['grupo_id']= $grupo_id;
        $data['materia_id']= $materia_id;
        $data['curso_id']= $curso_id;
        if($data['evaluacion_id']){
            $this->load->model('evaluaciones_model');
            $this->load->model('alumnos_evaluaciones_model');
            if($this->alumnos_evaluaciones_model->borrar_alumnos_evaluaciones_by_evaluacion_id($data)){
                $alumnos_evaluaciones_borrado=TRUE;
            }else{
                $alumnos_evaluaciones_borrado=FALSE;
            }
            if($alumnos_evaluaciones_borrado && $this->evaluaciones_model->borrar_evaluacion_by_id($data)){
                $data['mensaje'] = "evaluacion eliminada correctamente";
                $this->load->model('evaluaciones_model');
                $data['evaluaciones']=$this->evaluaciones_model->get_all_evaluaciones_by_curso_id($data);
                $this->load->view('mostrar_evaluaciones_view',$data);
            }else{
                $this->load->model('evaluaciones_model');
                $data['evaluaciones']=$this->evaluaciones_model->get_all_evaluaciones_by_curso_id($data);
                $data['mensaje'] = "error al borrar evaluaciones";
                $this->load->view('mostrar_evaluaciones_view',$data);
            }

        }else{
            $data['mensaje'] = "falta parametro rubrica_id";
            $this->load->view('mostrar_evaluaciones_view',$data);
        }

}

    /////////////////////////fin para agregar evaluacion///////////////////////////////////////////7





   ///////////////////////////inicio Evaluar alumnos//////////////////////////////////////////////////////////

    public function mostrar_evaluaciones_alumnos($curso_id,$grupo_id,$materia_id){
        $data['grupo_id']= $grupo_id;
        $data['materia_id']= $materia_id;
        $data['curso_id']= $curso_id;
        $this->load->model('alumnos_model');
        $this->load->model('alumnos_cursos_model');
        
        $data['alumnos']=$this->alumnos_cursos_model->get_all_alumnos_by_curso_id($data);
        $data['evaluaciones']= $this->alumnos_model->get_all_evaluaciones_by_curso_id($data);
        $data['numero_evaluaciones'] = count($data['evaluaciones']);

        foreach($data['alumnos'] as $alumno){
            $alumno['curso_id'] = $data['curso_id'];
            $data['objetos'][]=$data['alumno']=$this->alumnos_model->get_all_by_alumno_id($alumno);
            }
        
          ////promediar
        $this->load->model('alumnos_evaluaciones_model');
        $calificaciones=array();
        $this->load->model('evaluaciones_model');
        $data['rubrica_evaluacion_id']=$this->evaluaciones_model->get_una_evaluacion_por_rubrica();
        $data['mensaje'] = "";
        $calificaciones=0;
        $calificaciones_finales=array();
        foreach($data['alumnos'] as $alumno){ 
            foreach($data['rubrica_evaluacion_id'] as $rubrica_id){
                $alumno['curso_id'] = $data['curso_id'];
                $resultado=$this->alumnos_evaluaciones_model->promediar_by_alumno_by_id_rubrica_id($rubrica_id,$alumno);//
                $calificaciones =$calificaciones + $resultado[0]['AVG(alumnos_evaluaciones.calificacion)*(rubrica_evaluacion.ponderacion/100)'];
            }

            $calificaciones_finales[] = array('calificacion_final'=>$calificaciones,'alumno'=>$alumno['alumno'],'apellido_paterno'=>$alumno['apellido_paterno'],'apellido_materno'=>$alumno['apellido_materno']);
            $calificaciones=0; 
        }
        $data['calificaciones_finales']= $calificaciones_finales;
        $this->load->view('mostrar_evaluaciones_alumnos_view',$data);
    }

  
    public function mostrar_calificar_form($curso_id,$grupo_id,$materia_id,$alumno_id,$alumnos_evaluaciones_id){
            $data['grupo_id'] = $grupo_id;
            $data['materia_id']= $materia_id;
            $data['alumno_id'] = $alumno_id;
            $data['alumnos_evaluaciones_id'] = $alumnos_evaluaciones_id;
            $data['curso_id'] = $curso_id;
            $data['mensaje'] = "";
            if($data['grupo_id'] && $data['materia_id'] && $data['alumno_id'] && $data['alumnos_evaluaciones_id'] && $data['curso_id']){
                $this->load->view('mostrar_calificar_alumnos_form_view',$data);
            }else{
                $data['mensaje'] = "faltan parametros";
                $this->load->view('mostrar_evaluaciones_alumnos_view',$data);
            }
    
    }
    public function update_calificacion(){
        if($this->validar_calificacion_alumno()){
            $data['curso_id']= $this->input->post('curso_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['alumno_id']= $this->input->post('alumno_id');
            $data['calificacion']= $this->input->post('calificacion');
            $data['alumnos_evaluaciones_id']= $this->input->post('alumnos_evaluaciones_id');
            $this->load->model('alumnos_evaluaciones_model');
                if($this->alumnos_evaluaciones_model->update_calificacion($data)){
                    $data['mensaje'] = "calificacion insertada correctamente";
                    $this->load->view('mostrar_calificar_alumnos_form_view',$data);
                }else{
                    $data['mensaje'] = "error al insertar calificacion";
                    $this->load->view('mostrar_calificar_alumnos_form_view',$data);

                }
        }else{
            $data['curso_id']= $this->input->post('curso_id');
            $data['grupo_id']= $this->input->post('grupo_id');
            $data['materia_id']= $this->input->post('materia_id');
            $data['curso_id']= $this->input->post('curso_id');
            $data['alumno_id']= $this->input->post('alumno_id');
            $data['alumnos_evaluaciones_id']= $this->input->post('alumnos_evaluaciones_id');
            $data['mensaje'] = validation_errors();
            $this->load->view('mostrar_calificar_alumnos_form_view',$data);
        } 
    }



    ////////////////////////////////////////fin evaluar alumnos //////////////////////////////////////////////////////


    private function validar_login_form(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('correo', 'correo', 'required|valid_email');
        if ($this->form_validation->run()){            
            return TRUE;
        }else{
            return FALSE;
        }
    }

    

    private function validar_rubrica_form(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ponderacion', 'ponderacion', 'required|numeric');
        $this->form_validation->set_rules('rubrica', 'rubrica', 'required');

        if ($this->form_validation->run()){            
            return TRUE;
        }else{
            return FALSE;
        }
    }

    private function validar_evaluacion_form(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('evaluacion', 'evaluacion', 'required');
        $this->form_validation->set_rules('rubrica_evaluacion_id', 'rubrica_evaluacion_id', 'required|numeric|integer');

        if ($this->form_validation->run()){            
            return TRUE;
        }else{
            return FALSE;
        }

    }

    private function validar_calificacion_alumno(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('alumno_id', 'alumno_id', 'required|numeric|integer');
        $this->form_validation->set_rules('calificacion', 'calificacion', 'required|numeric|less_than_equal_to[10]');

        if ($this->form_validation->run()){            
            return TRUE;
        }else{
            return FALSE;
        }

    }




}
