<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Evaluaciones</title>
    <style type="text/css">
        .container{
                display: flex;
                justify-content: flex-start;
                margin:1px;
        }
    </style>
    
</head>
<body> 
<a href="<?php echo base_url().'index.php/index/recibir_materia/'.$grupo_id.'/'.$materia_id?>">Menu materia</a>


        <?php if($numero_evaluaciones>0){?>

        <?= $mensaje; ?>
        <h1>Evaluaciones de los alumnos</h1>

        <div class="container">
        <table border="solid">
            
         <tr>   <th></th>
                <th colspan="<?= $numero_evaluaciones?>">Evaluaciones</th>
                
         </tr>
        <tr>
                <th>Alumnos</th>
                <?php foreach($evaluaciones as $evaluacion){ ?>
                       <th> <?= $evaluacion['evaluacion'];?> </th>
                <?php }?>
        </tr>
        <?php $num_pasada=TRUE;?>
        <?php foreach($objetos as $alumno){?>
        <tr>  
       
            <th><?= $alumno[0]['alumno'].' '.$alumno[0]['apellido_paterno'].' '.$alumno[0]['apellido_materno'];?></th>

                        
                    <?php foreach($alumno as $un_alumno){?>
                             <th>
                             <?php if($un_alumno['calificacion']==NULL){ ?>
                             <a href="<?php echo base_url().'index.php/index/mostrar_calificar_form/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$un_alumno['id'].'/'.$un_alumno['alumnos_evaluaciones_id']?>">calificar</a>
                             <?php $calificaciones[]= array('calificacion'=>$un_alumno['calificacion'],'rubrica_id'=>$un_alumno['rubrica_evaluacion_id'],'ponderacion'=>$un_alumno['ponderacion']);?>
                             <?php }else{?>
                                        
                                        <?php $calificaciones[]= array('calificacion'=>$un_alumno['calificacion'],'rubrica_id'=>$un_alumno['rubrica_evaluacion_id'],'ponderacion'=>$un_alumno['ponderacion']);?>       
                                        <a href="<?php echo base_url().'index.php/index/mostrar_calificar_form/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$un_alumno['id'].'/'.$un_alumno['alumnos_evaluaciones_id']?>"><?= $un_alumno['calificacion']; ?></a>
                             <?php }?>
                             </th> 
                             <?php }?>

                            
                           
                            
        </tr>             
                <?php }?>
                
               
                                
                                
                  
        </table>
        <table border="solid" float="rigth">
                <tr>
                        <th colspan="2">CALIFICACIONES FINALES</th>
                </tr>
                <tr>
                <th>Calificacion final</th>
                </tr>
                
                        <?php foreach($calificaciones_finales as $calificacion_final){?>
                                <tr>
        
                                <th><?= $calificacion_final['calificacion_final'];?></th>
                                </tr>
                        <?php }?>
                
        </table>
        </div>
            
        <?php }else {?>

                        <h1>Aún no hay evaluaciones registradas</h1>
                        <a href="<?php echo base_url().'index.php/index/mostrar_evaluaciones/'.$curso_id.'/'.$grupo_id.'/'.$materia_id ?>">Agregar nueva evaluacion</a>
                        <br><br>
                        <table border="solid">
                        <tr>
                                <th>Alumnos</th>
                        </tr>
                        <?php foreach($alumnos as $alumno){?>

                                <tr>
                                        <th><?= $alumno['alumno'].' '.$alumno['apellido_paterno'].' '.$alumno['apellido_materno'];?></th>
                                </tr>
                        <?php }?>
                        </table>
                       
        <?php }?>
           
        
</body>
        
</body>
</html>