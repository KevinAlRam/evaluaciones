<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Rubricas</title>
</head>
<body>
        <a href="<?php echo base_url().'index.php/index/recibir_materia/'.$grupo_id.'/'.$materia_id?>">Menu materia</a>
        <?= $mensaje; ?>
        <h1>Rubrica de la materia</h1>
        <table border="solid">
        <tr>
            <th>nombre</th>
            <th>ponderacion</th>
        </tr>
        <?php if(!empty($rubricas)){ ?>
        <?php foreach($rubricas as $rubrica){ ?>
        <tr>
            <td><?= $rubrica['rubrica'];?></td>
            <td><?= $rubrica['ponderacion'];?></td>
            <td><a href="<?php echo base_url().'index.php/index/editar_rubrica_view/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$rubrica['id']?>">Editar</a></td>
            <td><a href="<?php echo base_url().'index.php/index/borrar_rubrica/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$rubrica['id']?>">Borrar</a></td>
            
        </tr>
        <?php }?>
        <?php }?>
        
        </table>
        <br>
        <br>
        <br>
        <a href="<?php echo base_url().'index.php/index/mostrar_rubrica_form/'.$curso_id.'/'.$grupo_id.'/'.$materia_id?>"> Agregar rubrica </a>
</body>
</html>