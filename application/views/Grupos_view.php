<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Grupos</title>
</head>
<body>
    <?= $mensaje; ?>
    <h1>Grupos</h1>
    <ul>
    <?php foreach($grupos as $grupo){?>

            <li><a href="<?php echo base_url().'index.php/index/recibir_grupo/'.$grupo['id'] ?>"> <?= $grupo['grupo']; ?> </a></li>
            
    <?php }?>
    </ul>
</body>
</html>