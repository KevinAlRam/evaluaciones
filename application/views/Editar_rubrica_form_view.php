<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Editar rubrica</title>
</head>
<body>
        <?= $mensaje;?>
        <a href="<?php echo base_url().'index.php/index/recibir_menu_rubrica/'.$curso_id.'/'.$grupo_id.'/'.$materia_id?>">Rubricas</a>
        <h1>Editar rubrica</h1>
        <?= form_open('index/update_rubrica')?>

            <label for="">Nombre</label>
            <input type="text" name="rubrica" placeholder="ingrese nombre de la rubrica" value="<?php if(isset($rubrica['rubrica'])) echo $rubrica['rubrica']; ?>">
            <label for="">Ponderacion</label>
            <input type="text" name="ponderacion" placeholder="ingrese el valor en porcentaje" value="<?php if(isset($rubrica['ponderacion'])) echo $rubrica['ponderacion']; ?>">
            <input type="hidden" name="rubrica_id" value="<?php if(isset($rubrica['id'])) echo $rubrica['id'];?>">
            <input type="hidden" name="materia_id" value="<?php if(isset($materia_id)) echo $materia_id?>">
            <input type="hidden" name="grupo_id" value="<?php if(isset($grupo_id)) echo $grupo_id?>">
            <input type="hidden" name="curso_id" value="<?php if(isset($curso_id)) echo $curso_id?>">

            <input type="submit" name="submit">

        <?= form_close();?>
    
</body>
</html>