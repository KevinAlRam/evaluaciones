<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Editar evaluacion</title>
</head>
<body>
        <?= $mensaje;?>
        <a href="<?php echo base_url().'index.php/index/mostrar_evaluaciones/'.$curso_id.'/'.$grupo_id.'/'.$materia_id?>">Evaluaciones</a>
        <h1>Editar evaluacion</h1>
        <?= form_open('index/editar_evaluacion')?>

            <label for="">Evaluacion</label>
            <input type="text" name="evaluacion" placeholder="ingrese el nombre de la evaluacion" value="<?php if(isset($evaluacion['evaluacion'])) echo $evaluacion['evaluacion'];?>">
            <h2>Selecciona la rubrica a la que pertenece la evaluacion</h2>
            <select name="rubrica_evaluacion_id">
                <?php foreach($rubricas as $rubrica){?>
                    <option value="<?= $rubrica['id']?>" > <?= $rubrica['rubrica']; ?> </option>
                <?php }?>
            </select>
            <input type="hidden" name="materia_id" value="<?=$materia_id?>">
            <input type="hidden" name="grupo_id" value="<?=$grupo_id?>">
            <input type="hidden" name="curso_id" value="<?=$curso_id?>">
            <input type="hidden" name="evaluacion_id" value="<?=$evaluacion_id?>">
            <input type="submit" name="submit">
            

        <?= form_close();?>
</body>
</html>