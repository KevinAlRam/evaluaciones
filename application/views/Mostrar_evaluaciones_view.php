<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Evaluaciones</title>
</head>
<body>  
        <a href="<?php echo base_url().'index.php/index/recibir_materia/'.$grupo_id.'/'.$materia_id?>">Menu materia</a>
        <?= $mensaje; ?>
        <h1>Evaluaciones</h1>
        <table border="solid">
        <tr>
            <th>Evaluacion</th>
            <th>Rubrica</th>
        </tr>
        <?php if(!empty($evaluaciones)){ ?>
        <?php foreach($evaluaciones as $evaluacion){ ?>
        <tr>
            <td><?= $evaluacion['evaluacion'];?></td>
            <td><?= $evaluacion['rubrica'];?></td>
            <td><a href="<?php echo base_url().'index.php/index/editar_evaluacion_form/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$evaluacion['id']?>">Editar</a></td>
            <td><a href="<?php echo base_url().'index.php/index/borrar_evaluacion/'.$curso_id.'/'.$grupo_id.'/'.$materia_id.'/'.$evaluacion['id']?>">Borrar</a></td>
            
        </tr>
        <?php }?>
        <?php }?>
        
        </table>
        <br>
        <br>
        <br>
        <a href="<?php echo base_url().'index.php/index/mostrar_agregar_evaluacion_form/'.$curso_id.'/'.$grupo_id.'/'.$materia_id?>"> Agregar evaluacion</a>
</body>
        
</body>
</html>