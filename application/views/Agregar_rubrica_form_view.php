<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Agregar rubrica form</title>
</head>
<body>
        <?= $mensaje;?>
        <a href="<?php echo base_url().'index.php/index/recibir_menu_rubrica/'.$curso_id.'/'.$grupo_id.'/'.$materia_id?>">Rubricas</a>
        <h1>Agregar rubrica</h1>
        <?= form_open('index/insertar_rubrica')?>

            <label for="">Nombre</label>
            <input type="text" name="rubrica" placeholder="ingrese nombre de la rubrica">
            <label for="">Ponderacion</label>
            <input type="text" name="ponderacion" placeholder="ingrese el valor en porcentaje">
            <input type="hidden" name="materia_id" value="<?=$materia_id?>">
            <input type="hidden" name="grupo_id" value="<?=$grupo_id?>">
            <input type="hidden" name="curso_id" value="<?=$curso_id?>">
            <input type="submit" name="submit">

        <?= form_close();?>
</body>
</html>