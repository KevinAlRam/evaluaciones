<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php $this->load->helper('url');?>
    <title>Menu Materia</title>
</head>
<body>
    <? $mensaje; ?>
    <h1>Menú de la materia</h1>
    <ul>
        <li><a href="<?php echo base_url().'index.php/index/recibir_menu_rubrica/'.$curso_id.'/'.$grupo_id.'/'.$materia_id ?>">Rubricas</a></li>
        <li><a href="<?php echo base_url().'index.php/index/mostrar_evaluaciones/'.$curso_id.'/'.$grupo_id.'/'.$materia_id ?>">Agregar Evaluacion</a></li>
        <li><a href="<?php echo base_url().'index.php/index/mostrar_evaluaciones_alumnos/'.$curso_id.'/'.$grupo_id.'/'.$materia_id ?>">Evaluar Alumnos</a></li>
    </ul>

</body>
</html>