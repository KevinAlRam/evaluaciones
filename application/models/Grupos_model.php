<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }

    public function get_grupos_by_profesor_correo($data){
        $query= $this->db->query("SELECT DISTINCT profesores.correo, grupos.grupo, profesores.id, grupos.id FROM profesores INNER JOIN curso ON curso.profesor_id = profesores.id INNER JOIN grupos ON curso.grupo_id = grupos.id WHERE profesores.correo = '".$data['correo']."' ");
        return $query->result_array();
    }

    public function get_materias_by_grupo_id($data){
        $query= $this->db->query("SELECT
        grupos.id,
        materias.materia,
        materias.id 
        FROM
        grupos
        INNER JOIN curso ON curso.grupo_id = grupos.id
        INNER JOIN materias ON curso.materia_id = materias.id
        WHERE
        grupos.id = '".$data['grupo_id']."' ");
        return $query->result_array();
    }

}

