<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos_evaluaciones_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }
    public function update_calificacion($data){
        $query= $this->db->query("UPDATE alumnos_evaluaciones SET calificacion = '".$data['calificacion']."'
        WHERE id =  '".$data['alumnos_evaluaciones_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public function promediar_by_alumno_by_id_rubrica_id($rubrica_id,$alumno){
        $query= $this->db->query("SELECT
        AVG(alumnos_evaluaciones.calificacion)*(rubrica_evaluacion.ponderacion/100)
        FROM
        curso
        INNER JOIN alumnos_cursos ON alumnos_cursos.curso_id = curso.id
        INNER JOIN alumnos ON alumnos_cursos.alumno_id = alumnos.id
        INNER JOIN alumnos_evaluaciones ON alumnos_evaluaciones.alumno_id = alumnos.id
        INNER JOIN evaluaciones ON alumnos_evaluaciones.evaluacion_id = evaluaciones.id
        INNER JOIN rubrica_evaluacion ON rubrica_evaluacion.curso_id = curso.id AND evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        WHERE
        alumnos.id ='".$alumno['alumno_id']."' AND
        rubrica_evaluacion.id = '".$rubrica_id['rubrica_evaluacion_id']."' AND curso.id= '".$alumno['curso_id']."' ");
       
        return $query->result_array();

    }
    public function get_all_evaluaciones_calificaciones_by_alumno_id(){
        $query= $this->db->query("SELECT
        rubrica_evaluacion.id,
        rubrica_evaluacion.ponderacion,
        evaluaciones.evaluacion,
        evaluaciones.id,
        alumnos_evaluaciones.calificacion,
        alumnos.alumno
        FROM
        rubrica_evaluacion
        INNER JOIN evaluaciones ON evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        INNER JOIN alumnos_evaluaciones ON alumnos_evaluaciones.evaluacion_id = evaluaciones.id
        INNER JOIN alumnos ON alumnos_evaluaciones.alumno_id = alumnos.id
        ORDER BY
        alumnos.id ASC ");
        return $query->result_array();

    }
    public function insertar_evaluacion_x_alumno($alumno,$ultimo_id){
        $query= $this->db->query("INSERT INTO alumnos_evaluaciones(alumno_id, evaluacion_id) 
        VALUES('".$alumno['alumno_id']."', '".$ultimo_id['ultima_evaluacion_id']."')");
        if($query){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }
    public function borrar_alumnos_evaluaciones_by_evaluacion_id($data){
        $query= $this->db->query(" DELETE FROM alumnos_evaluaciones WHERE evaluacion_id = '".$data['evaluacion_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
   
}
