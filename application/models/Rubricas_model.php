<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubricas_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }

    public function get_all_rubricas(){
        $query= $this->db->query("SELECT rubrica_evaluacion.id, rubrica_evaluacion.rubrica, rubrica_evaluacion.ponderacion FROM rubrica_evaluacion");
        return $query->result_array();
    }

    public function insertar_rubrica($data){
        $query= $this->db->query("INSERT INTO rubrica_evaluacion(rubrica, ponderacion, curso_id) VALUES('".$data['rubrica']."','".$data['ponderacion']."', '".$data['curso_id']."')");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    public function get_curso_id_by_materia_grupo_id($data){
        $query= $this->db->query("SELECT curso.id FROM curso WHERE curso.materia_id = '".$data['materia_id']."' AND curso.grupo_id = '".$data['grupo_id']."'");  
        return $query->result_array(); 
    }

    public function get_rubrica_by_curso_id($data){
        $query= $this->db->query("SELECT
        *
        FROM
        rubrica_evaluacion
        WHERE
        rubrica_evaluacion.curso_id = '".$data['curso_id']."' ");
        return $query->result_array();
    }

    public function get_rubrica_by_id($data){
        $query= $this->db->query("SELECT * FROM rubrica_evaluacion WHERE rubrica_evaluacion.id = '".$data['rubrica_id']."'");
        if($query){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }
    public function update_rubrica_by_id($data){
        $query= $this->db->query("UPDATE rubrica_evaluacion SET rubrica = '".$data['rubrica']."', ponderacion = '".$data['ponderacion']."' WHERE rubrica_evaluacion.id = '".$data['rubrica_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function borrar_rubrica_by_id($data){
        $query= $this->db->query(" DELETE FROM rubrica_evaluacion WHERE rubrica_evaluacion.id = '".$data['rubrica_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
     
    }
    public function get_rubrica_by_rubrica_evaluacion_id($data){
        $query= $this->db->query("SELECT DISTINCT rubrica_evaluacion.rubrica FROM rubrica_evaluacion
        INNER JOIN evaluaciones ON evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        WHERE evaluaciones.rubrica_evaluacion_id = '".$data['rubrica_evaluacion_id']."' ");  
        return $query->result_array(); 
    }
    public function get_suma_de_ponderaciones_by_curso_id($data){
        $query= $this->db->query("SELECT
        Sum(rubrica_evaluacion.ponderacion)
        FROM
        rubrica_evaluacion
        INNER JOIN curso ON rubrica_evaluacion.curso_id = curso.id
        WHERE
        curso.id = '".$data['curso_id']."' ");  
        return $query->result_array(); 
    }




}

