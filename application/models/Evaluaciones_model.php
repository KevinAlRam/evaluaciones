<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluaciones_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }
    
    public function get_all_evaluaciones_by_curso_id($data){
        $query= $this->db->query("SELECT evaluaciones.id, evaluaciones.evaluacion, evaluaciones.rubrica_evaluacion_id, 
        rubrica_evaluacion.curso_id,rubrica_evaluacion.rubrica
        FROM rubrica_evaluacion INNER JOIN evaluaciones ON evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        WHERE rubrica_evaluacion.curso_id = '".$data['curso_id']."'");
        return $query->result_array();
    }

    public function insertar_evaluacion($data){
        $query= $this->db->query("INSERT INTO evaluaciones(evaluacion, rubrica_evaluacion_id) 
        VALUES('".$data['evaluacion']."','".$data['rubrica_evaluacion_id']."')");
        if($query){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }

    }
    public function get_evaluacion_by_id($data){
        $query= $this->db->query("SELECT
        evaluaciones.id,
        evaluaciones.evaluacion,
        evaluaciones.rubrica_evaluacion_id
        FROM
        evaluaciones
        WHERE
        evaluaciones.id = '".$data['evaluacion_id']."'");  
        return $query->result_array(); 
    }
    
    public function update_evaluacion_by_id($data){
        $query= $this->db->query("UPDATE evaluaciones SET evaluacion = '".$data['evaluacion']."', rubrica_evaluacion_id = '".$data['rubrica_evaluacion_id']."' 
        WHERE id = '".$data['evaluacion_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function get_evaluaciones_by_rubrica_evaluacion_id($data){
        $query= $this->db->query("SELECT
        evaluaciones.id,
        evaluaciones.evaluacion,
        evaluaciones.rubrica_evaluacion_id
        FROM
        evaluaciones
        WHERE
        evaluaciones.rubrica_evaluacion_id = '".$data['rubrica_evaluacion_id']."' ");  
        return $query->result_array(); 
    }
    public function borrar_evaluaciones_by_rubrica_evaluaciones_id($data){
        $query= $this->db->query(" DELETE FROM evaluaciones WHERE rubrica_evaluacion_id = '".$data['rubrica_evaluacion_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function borrar_evaluacion_by_id($data){
        $query= $this->db->query(" DELETE FROM evaluaciones WHERE evaluaciones.id = '".$data['evaluacion_id']."' ");
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function count_evaluaciones_by_rubrica_evaluacion_id($data){
        $query= $this->db->query("SELECT Count(rubrica_evaluacion_id) FROM evaluaciones WHERE rubrica_evaluacion_id= '".$data['rubrica_evaluacion_id']."' ");
        return $query->result_array();  
    }
    public function get_rubrica_evaluacion_id(){
        $query= $this->db->query("SELECT DISTINCT evaluaciones.rubrica_evaluacion_id FROM evaluaciones");
        return $query->result_array(); 
        

    }
    public function get_rubrica_evaluacion_id_mas_repetido(){
        $query= $this->db->query("SELECT Max(evaluaciones.rubrica_evaluacion_id) FROM evaluaciones");
        return $query->result_array(); 
    }
    public function get_maximo_rubrica_evaluacion_id_repetido($data){
        $query= $this->db->query("SELECT Count(evaluaciones.rubrica_evaluacion_id) FROM evaluaciones
        WHERE evaluaciones.rubrica_evaluacion_id = '".$data['rubrica_evaluacion_id']."' ");
        return $query->result_array(); 
    }

    public function get_all_rubrica_evaluacion_id(){
        $query= $this->db->query("SELECT DISTINCT evaluaciones.rubrica_evaluacion_id FROM evaluaciones ");
        return $query->result_array(); 
    }

    public function get_una_evaluacion_por_rubrica(){
        $query= $this->db->query("SELECT rubrica_evaluacion_id FROM evaluaciones GROUP BY rubrica_evaluacion_id  ");
        return $query->result_array();
    } 

    

}

