<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesores_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }
    
    public function get_profesor_by_correo($data){
        $query = $this->db->query("SELECT * FROM profesores WHERE profesores.correo = '".$data['correo']."'");
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return FALSE;
        }
    }

}

