<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos_cursos_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }
    public function get_all_alumnos_by_curso_id($data){
        $query= $this->db->query("SELECT
        alumnos_cursos.alumno_id,
        alumnos_cursos.id,
        alumnos.alumno,
        alumnos.apellido_paterno,
        alumnos.apellido_materno
        FROM
        alumnos_cursos
        INNER JOIN alumnos ON alumnos_cursos.alumno_id = alumnos.id
        WHERE
        alumnos_cursos.curso_id = '".$data['curso_id']."' ");
        return $query->result_array();
    }
    
   

    

}

