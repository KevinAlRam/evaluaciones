<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos_model extends CI_Model {

    public function __construct(){
        parent:: __construct();
        $this->load->database();
    }
    public function asignar_calificacion_final($data){
       
            $query= $this->db->query("UPDATE alumnos SET calificacion_final = '".$data['calificacion_final']."'
            WHERE alumnos.id = '".$data['alumno_id']."' ");
            if($query){
                return TRUE;
            }else{
                return FALSE;
            }
    }
    /////////////////////77funciones nuevas////////////7//////////////////////////////////////////7
    public function get_all_alumnos_by_curso_id($data){
        $query= $this->db->query(" SELECT
        alumnos_cursos.alumno_id,
        alumnos_cursos.id,
        alumnos.alumno,
        alumnos.apellido_paterno,
        alumnos.apellido_materno
        FROM
        alumnos_cursos
        INNER JOIN alumnos ON alumnos_cursos.alumno_id = alumnos.id
        WHERE
        alumnos_cursos.curso_id = '".$data['curso_id']."'
        ORDER BY
        alumnos.id ASC  "); 
        return $query->result_array();
    }

    public function get_all_evaluaciones(){
        $query= $this->db->query("SELECT
        evaluaciones.evaluacion,
        evaluaciones.rubrica_evaluacion_id,
        evaluaciones.id,
        alumnos_evaluaciones.alumno_id,
        alumnos_evaluaciones.calificacion
        FROM
        evaluaciones
        INNER JOIN alumnos_evaluaciones ON alumnos_evaluaciones.evaluacion_id = evaluaciones.id
        ORDER BY
        alumnos_evaluaciones.alumno_id ASC ");
        return $query->result_array();
    }
    public function get_all_evaluaciones_by_alumno_id($data){
        $query= $this->db->query("SELECT
        evaluaciones.evaluacion,
        evaluaciones.rubrica_evaluacion_id,
        evaluaciones.id,
        alumnos_evaluaciones.alumno_id,
        alumnos_evaluaciones.calificacion
        FROM
        evaluaciones
        INNER JOIN alumnos_evaluaciones ON alumnos_evaluaciones.evaluacion_id = evaluaciones.id
        WHERE
        alumnos_evaluaciones.alumno_id = '".$data['id']."'
        ORDER BY
        alumnos_evaluaciones.alumno_id ASC");
        return $query->result_array();
    }
    public function get_alumno_by_alumno_id($data){
        $query= $this->db->query("SELECT
        alumnos.id,
        alumnos.alumno,
        alumnos.apellido_paterno,
        alumnos.apellido_materno
        FROM
        alumnos
        WHERE
        alumnos.id = '".$data['id']."' ");
        return $query->result_array();
    }

    public function get_all_by_alumno_id($data){
        $query= $this->db->query("SELECT
        curso.id,
        alumnos_cursos.alumno_id,
        alumnos.alumno,
        alumnos.apellido_paterno,
        alumnos.apellido_materno,
        alumnos_evaluaciones.calificacion,
        evaluaciones.evaluacion,
        rubrica_evaluacion.ponderacion,
        alumnos_evaluaciones.evaluacion_id,
        alumnos_evaluaciones.id as alumnos_evaluaciones_id, 
        rubrica_evaluacion.id as rubrica_evaluacion_id 
        FROM
        curso
        INNER JOIN alumnos_cursos ON alumnos_cursos.curso_id = curso.id
        INNER JOIN alumnos ON alumnos_cursos.alumno_id = alumnos.id
        INNER JOIN alumnos_evaluaciones ON alumnos_evaluaciones.alumno_id = alumnos.id
        INNER JOIN evaluaciones ON alumnos_evaluaciones.evaluacion_id = evaluaciones.id
        INNER JOIN rubrica_evaluacion ON rubrica_evaluacion.curso_id = curso.id AND evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        WHERE
        alumnos.id = '".$data['alumno_id']."' AND curso.id= '".$data['curso_id']."'
        ORDER BY
        evaluaciones.id ASC");
        return $query->result_array();
    }

    function get_all_evaluaciones_by_curso_id($data){
        $query= $this->db->query("SELECT
        evaluaciones.evaluacion
        FROM
        evaluaciones
        INNER JOIN rubrica_evaluacion ON evaluaciones.rubrica_evaluacion_id = rubrica_evaluacion.id
        INNER JOIN curso ON rubrica_evaluacion.curso_id = curso.id
        WHERE
        curso.id = '".$data['curso_id']."'
        ORDER BY
        evaluaciones.id ASC");
        return $query->result_array();

    }
    
}
